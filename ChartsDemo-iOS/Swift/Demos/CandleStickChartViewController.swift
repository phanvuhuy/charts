//
//  CandleStickChartViewController.swift
//  ChartsDemo-iOS
//
//  Created by Jacob Christie on 2017-07-09.
//  Copyright © 2017 jc. All rights reserved.
//

import UIKit
import Charts

class CandleStickChartViewController: DemoBaseViewController {

    @IBOutlet var chartView: CandleStickChartView!
    @IBOutlet var sliderX: UISlider!
    @IBOutlet var sliderY: UISlider!
    @IBOutlet var sliderTextX: UITextField!
    @IBOutlet var sliderTextY: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Candle Stick Chart"
        self.options = [.toggleValues,
                        .toggleIcons,
                        .toggleGradient,
                        .toggleHighlight,
                        .animateX,
                        .animateY,
                        .animateXY,
                        .saveToGallery,
                        .togglePinchZoom,
                        .toggleAutoScaleMinMax,
                        .toggleShadowColorSameAsCandle,
                        .toggleShowCandleBar,
                        .toggleData]
        
        chartView.delegate = self
        chartView.candleDelegate = self
        
        chartView.chartDescription?.enabled = false
        
        chartView.dragEnabled = false
//        chartView.setScaleEnabled(true)
//        chartView.pinchZoomEnabled = true
        chartView.legend.enabled = false
//        chartView.legend.horizontalAlignment = .right
//        chartView.legend.verticalAlignment = .top
//        chartView.legend.orientation = .vertical
//        chartView.legend.drawInside = false
//        chartView.legend.font = UIFont(name: "HelveticaNeue-Light", size: 10)!
      
        chartView.leftAxis.labelFont = UIFont(name: "HelveticaNeue-Light", size: 10)!
      
        chartView.leftAxis.removeAllLimitLines()
        chartView.leftAxis.drawGridLinesEnabled = true
        chartView.xAxis.centerAxisLabelsEnabled = true


        chartView.rightAxis.enabled = false
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.labelFont = UIFont(name: "HelveticaNeue-Light", size: 10)!
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.axisMaximum = 7
        xAxis.axisMinimum = 0
        xAxis.granularity = 1
 
        xAxis.gridColor = UIColor.lightGray
        xAxis.gridLineDashLengths = [2, 2]
        
        sliderX.value = 7
        sliderY.value = 7
        slidersValueChanged(nil)
    }
    
    override func updateChartData() {
        if self.shouldHideData {
            chartView.data = nil
            return
        }
        
        self.setDataCount(Int(sliderX.value), range: UInt32(sliderY.value))
    }
    
    func setDataCount(_ count: Int, range: UInt32) {
        var yVals1 = (0...count).map { (i) -> CandleChartDataEntry in
            let high = Double.random(in: 5..<10)
            let low = Double.random(in: 1..<5)
            let open = high
            let close = low
            return CandleChartDataEntry(x: Double(i) + 0.5, shadowH: high, shadowL: low, open: open, close: close)
        }
        let emptyDataEntry = CandleChartDataEntry()
        
        
        var set1 = CandleChartDataSet(entries: yVals1, label: "Data Set")
        yVals1.insert(emptyDataEntry, at: 0)

        set1.axisDependency = .left
        set1.barSpace = 0.2
        set1.drawIconsEnabled = false
        set1.drawBarGradientEnabled = true
        set1.gradientPositions = [0, 100]
        set1.formLineWidth = 0.7
        set1.colors = [
            UIColor(red: 253/255, green: 199/255, blue: 111/255, alpha: 1),
            UIColor(red: 253/255, green: 125/255, blue: 111/255, alpha: 1)
        ]
        
        let data = CandleChartData(dataSet: set1)
      
        chartView.data = data
    }
    
    override func optionTapped(_ option: Option) {
        switch option {
        case .toggleShadowColorSameAsCandle:
            for set in chartView.data!.dataSets as! [CandleChartDataSet] {
                set.shadowColorSameAsCandle = !set.shadowColorSameAsCandle
            }
            chartView.notifyDataSetChanged()
        case .toggleShowCandleBar:
            for set in chartView.data!.dataSets as! [CandleChartDataSet] {
                set.showCandleBar = !set.showCandleBar
            }
            chartView.notifyDataSetChanged()
        default:
            super.handleOption(option, forChartView: chartView)
        }
    }
    
    // MARK: - Actions
    @IBAction func slidersValueChanged(_ sender: Any?) {
        sliderTextX.text = "\(Int(sliderX.value))"
        sliderTextY.text = "\(Int(sliderY.value))"
        
        self.updateChartData()
    }}

extension CandleStickChartViewController: CandleChartDelegate {
  func chartColumnRect(dict: [Int : [CGFloat]]) {
    print(dict)
  }
}
